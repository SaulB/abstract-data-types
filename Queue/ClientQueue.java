/**
 * <h1>Queue of clients.</h1>
 * The ClientQueue class is a single-server queuing system.
 *
 * @author Saul Boyd, 1534999
 */
public class ClientQueue {
	/**
	 * A node to hold the client arrival time and usage time.
	 * The Client implements a node that holds data for a client in a singly
	 * linked list.
	 */
	private class Client {
		// The arrival time for this client.
		private int arrTime;

		// The time this client will need when it gets to the end of the queue.
		private int useTime;

		// The next client in the queue.
		public Client next = null;

		/**
		 * Creates a new client.
		 * @param t The int to store the arrival time of this client.
		 * @param u The int to store the usage time of this client.
		 */
		public Client(int t, int u) {
			arrTime = t;
			useTime = u;
		}

		/**
		 * Get the arrival time of this client.
		 * @return int The arrival time.
		 */
		public int getArrTime() {
			return arrTime;
		}

		/**
		 * Get the usage time of this client.
		 * @return int The usage time.
		 */
		public int getUseTime() {
			return useTime;
		}
	}

	// The head (start) of the queue.
	private Client head = null;

	// The count of clients enqueued.
	private int size = 0;

	/**
	 * Add a client to the end of the queue.
	 * @param t The arrival time for this client.
	 * @param u The usage time for this client.
	 * @throws IllegalArgumentException If t is less than 0.
	 * @throws IllegalArgumentException If u is less than 1.
	 */
	public void enqueue(int t, int u) {
		if (t < 0)
			throw new IllegalArgumentException(
				"The arrival time (t) should be non-negative."
			);

		if (u < 1)
			throw new IllegalArgumentException(
				"The usage time (u) should be greater than 0."
			);

		Client client = new Client(t, u);

		size++;

		if (head == null) {
			head = client;

			return;
		}

		getTail().next = client;

		return;
	}

	/**
	 * Remove the client at the head (start) of the queue.
	 * Does nothing if the queue is empty.
	 */
	public void dequeue() {
		if (head == null)
			return;

		head = head.next;

		size--;
	}

	/**
	 * View the arrival time of the client at the head of the queue.
	 * @return int The arrival time of the client at the head.
	 * @throws NullPointerException If queue is empty.
	 */
	public int peekArrive() {
		if (head == null)
			throw new NullPointerException(
				"Cannot peek the arrival time of an empty queue."
			);

		return head.getArrTime();
	}

	/**
	 * View the usage time of the client at the head of the queue.
	 * @return int The usage time of the client at the head.
	 * @throws NullPointerException If queue is empty.
	 */
	public int peekUsage() {
		if (head == null)
			throw new NullPointerException(
				"Cannot peek the usage time of an empty queue."
			);

		return head.getUseTime();
	}

	/**
	 * Check to see if the queue is empty.
	 * @return boolean True if the queue has no clients otherwise false.
	 */
	public boolean isEmpty() {
		return length() == 0;
	}

	/**
	 * Get the total number of clients in this queue.
	 * @return int The number of clients enqueued.
	 */
	public int length () {
		return size;
	}

	/**
	 * Dump the the usage time and arrival time from each client in the queue
	 * in order.
	 */
	public void dump() {
		if (head == null)
			return;

		Client currNode = head;
		int i = 0;

		while (currNode != null) {
			System.out.println(
				"Index: " + i + " Usage: " + currNode.getUseTime()
				+ " Arrive: " + currNode.getArrTime()
			);

			i++;
			currNode = currNode.next;
		}
	}

	/**
	 * Get the tail (end) of the queue.
	 * @return Client The client who is at the tail of the queue.
	 */
	private Client getTail() {
		if (head == null)
			return null;

		Client tail = head;

		// Traverse to the end of the queue
		while(tail.next != null)
			tail = tail.next;

		return tail;
	}
}
