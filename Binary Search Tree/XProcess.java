import java.io.File;
import java.util.Scanner;

/**
 * <h1>XProcess</h1>
 * The XProcess application reads a transaction file and records the data in a
 * Binary Search Tree.
 *
 * @author Saul Boyd, 1534999
 */
public class XProcess {
	// Types of transactions.
	private static enum TransactionType {
		// A deposit transaction.
		DEPOSIT,

		// A Withdrawal transaction.
		WITHDRAW,

		// An account closure.
		CLOSE;
	}

	// The Binary search tree.
	private static BankBST bst;

	/**
	 * The entry point of the program.
	 * <p>
	 * Open and read the file given as an argument and proccess the list of
	 * transactions contained within that file.
	 * @param args An array that holds the filename that the user has passed.
	 */
	public static void main(String [] args){
		File file;
		Scanner sc;

		bst = new BankBST();

		// Check that an argument was provided
		if (args.length == 0){
			System.err.println(
				"Usage: java XProcess FILE\n"
				+ "  FILE: A plain text document containing transaction data."
			);

			return;
		}

		// Attempt to open and read the file.
		try {
			file = new File(args[0]);
			sc = new Scanner(file);
		} catch (Exception e) {
			System.err.println("Error: Unable to read file.\n" + e);
			return;
		}

		// Proccess the transactions
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] tokens = line.split(" ");

			// We have more or less tokens ignore line, assuming more tokens
			// means data is possibly incorrect.
			if (tokens.length != 3)
				continue;

			int key;
			TransactionType type;
			float magnitude = 0f;

			// Validate and extract the data
			try {
				key = extractKey(tokens[0]);
				type = extractType(tokens[1]);

				// If the transaction is a closure we don't need the magnitude.
				if (type != TransactionType.CLOSE)
					magnitude = extractMagnitude(tokens[2]);
			} catch (Exception e) {
				// Don't care why it went wrong - just that it did.
				continue;
			}


			// Must print any removals before the tree is modified.
			if (type == TransactionType.CLOSE && bst.find(key) != null)
				System.out.println(bst.getPath(key) + type);

			executeTransaction(key, type, magnitude);

			// Must print any potiential additions after the tree is modified.
			if (type != TransactionType.CLOSE)
				System.out.println(bst.getPath(key) + type);
		}

		// Output the result of the tree.
		System.out.println("RESULT");
		bst.dump();
	}

	/**
	 * Execute a transaction and update the BST.
	 * @param key The target of the transaction.
	 * @param type The transaction type.
	 * @param magnitude The magnitude of the transaction.
	 * @throws IllegalArgumentException If magnitude is less than 0.
	 */
	private static void executeTransaction(
		int key,
		TransactionType type,
		float magnitude
	) {
		if (magnitude < 0)
			throw new IllegalArgumentException(
				"The magnitude parameter should be non-negative."
			);

		switch (type) {
			case CLOSE:
				bst.remove(key);
				break;

			case DEPOSIT:
				updateBalance(key, magnitude);
				break;

			case WITHDRAW:
				updateBalance(key, -magnitude);
				break;
		}
	}

	/**
	 * Find and update a balance in the BST by key (account number).
	 * A new account will be created if one cannot be found.
	 * @param key The target of the transaction.
	 * @param amount The amount to alter or set the account to.
	 */
	private static void updateBalance(int key, float amount) {
		Account account = bst.find(key);

		if (account == null) {
			account = new Account(key, amount);

			try {
				bst.add(account);
			} catch (Exception e) {
				// This should not be a case if the methods are being used
				// correctly, log and exit for debugging if something went
				// wrong.
				System.err.println("Error adding account \'" + key
					+ "\' to BST.\n" + e);

				System.exit(0);
			}

		} else {
			account.setBalance(amount);
		}
	}

	/**
	 * Validate and extract the key (account number) from a string.
	 * @param token The token holding the key.
	 * @return int The extracted key.
	 * @throws Exception If token is not an int or is negative.
	 */
	private static int extractKey(String token) throws Exception {
		int key = Integer.parseInt(token);

		if (key < 0)
			throw new Exception("Key should be non-negative.");

		return key;
	}

	/**
	 * Validate and extract the type (transaction type character) from a string.
	 * @param token The token holding the type.
	 * @return TransactionType The extracted transaction type.
	 * @throws Exception If token is not a valid type.
	 */
	private static TransactionType extractType(String token) throws Exception {
		if (token.length() != 1)
			throw new Exception("Type should only be one character long.");

		char type = token.charAt(0);

		if ("cdw".indexOf(type) == -1)
			throw new Exception("Type should be one of: c, d or w.");

		if (type == 'c')
			return TransactionType.CLOSE;

		if (type == 'd')
			return TransactionType.DEPOSIT;

		return TransactionType.WITHDRAW;
	}

	/**
	 * Validate and extract the magnitude (amount of money) from a string.
	 * @param token The token holding the magnitude.
	 * @return float The extracted key.
	 * @throws Exception If token is not an float or is negative.
	 */
	private static float extractMagnitude(String token) throws Exception {
		float magnitude = Float.parseFloat(token);

		if (magnitude < 0)
			throw new Exception("Magnitude should be non-negative.");

		return magnitude;
	}
}
