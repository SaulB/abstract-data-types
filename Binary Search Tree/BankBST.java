/**
 * <h1>Binary Search Tree of Accounts</h1>
 * The BankBST class is for creating and manipulating a Binary Search Tree of
 * Accounts.
 *
 * @author Saul Boyd, 1534999
 */
public class BankBST {
	/**
	 * This Node class holds the account infomation along with the left and
	 * right nodes for a binary search tree.
	 */
	private class Node {
		// The account information for this node.
		public Account account;

		// The left node (values less than)
		public Node left = null;

		// The right node (values greater than)
		public Node right = null;

		/**
		 * Create a new node with account infomation.
		 * @param account The account infomation to associate with this node.
		 */
		public Node(Account account) {
			this.account = account;
		}

		/**
		 * Get the number of nodes this Node is directly linked to.
		 * @return The count of nodes.
		 */
		public int subtreeCount() {
			int i = left == null ? 0 : 1;

			return i + (right == null ? 0 : 1);
		}
	}

	// The root of the tree.
	private Node root = null;

	/**
	 * Add an existing account to the BST.
	 * @param account The account to add.
	 * @throws Exception If the account is already contained in the BST.
	 */
	public void add(Account account) throws Exception {
		root = add(root, new Node(account));
	}

	/**
	 * Find an account by key (account number).
	 * @param accountKey The key to find the account by.
	 * @return The found Account or <code>null</code> if not found.
	 */
	public Account find(int accountKey) {
		Node foundNode = find(root, accountKey);

		return foundNode == null ? null : foundNode.account;
	}

	/**
	 * Remove an account from the BST by key (account number).
	 * Does nothing if the account is not in the BST.
	 * @param accountKey The key for the account to be removed.
	 */
	public void remove(int accountKey) {
		root = remove(root, accountKey);
	}

	/**
	 * Get the path from the root to the node with account key.
	 * @param accountKey The key to geth the path to.
	 * @return The whitespace separated string of all account keys.
	 */
	public String getPath(int accountKey) {
		return getPath(root, accountKey);
	}

	/**
	 * Print the entire tree in order to the standard output.
	 */
	public void dump() {
		dump(root);
	}

	// --------------------------- Recursive Methods ---------------------------

	/**
	 * Recursively add a new subnode to a node.
	 * @param node The node to add the new node to.
	 * @param newNode The new subnode to add to the node.
	 * @return The original node with the new subnode added.
	 * @throws Exception If the account is already contained in the node.
	 */
	private Node add(Node node, Node newNode) throws Exception {
		if (node == null)
			return newNode;

		int key = node.account.getKey();
		int newKey = newNode.account.getKey();

		if (newKey < key) {
			node.left = add(node.left, newNode);
			return node;
		}

		if (newKey > key) {
			node.right = add(node.right, newNode);
			return node;
		}

		throw new Exception("Attempted to create node with duplicate key.");
	}

	/**
	 * Recursively search a node by account key.
	 * @param node The node to search.
	 * @param accountKey The key to search for.
	 * @return The node that holds an account with the key.
	 */
	private Node find (Node node, int accountKey) {
		if (node == null || node.account.getKey() == accountKey)
			return node;

		Node next = accountKey < node.account.getKey() ? node.left : node.right;

		return find(next, accountKey);
	}

	/**
	 * Recursively remove a node from another by account key.
	 * @param node The node to remove from.
	 * @param accountKey The key to search for.
	 * @return The node without the node with account key.
	 */
	private Node remove(Node node, int accountKey) {
		if (node == null)
			return null;

		if (accountKey < node.account.getKey()) {
			node.left = remove(node.left, accountKey);
			return node;
		}

		if (accountKey > node.account.getKey()) {
			node.right = remove(node.right, accountKey);
			return node;
		}

		// The node has been found by this point.
		if (node.subtreeCount() == 0)
			return null;

		if (node.subtreeCount() == 1)
			return node.left == null ? node.right : node.left;

		// The node has been found with 2 subtrees.
		Node successor = getMinNode(node.right);

		// Modify the original nodes data.
		node.account = successor.account;

		// Remove the successor
		node.right = remove(node.right, successor.account.getKey());

		return node;
	}

	/**
	 * Recursively get the path from a node to one with account key
	 * @param node The node to get the path from.
	 * @param accountKey The the key of the targeted node.
	 * @return The whitespace separated string of all account keys.
	 */
	private String getPath(Node node, int accountKey) {
		if (node == null)
			return "";

		int key = node.account.getKey();

		if (accountKey == key)
			return key + " ";

		Node next = accountKey < key ? node.left : node.right;

		return key + " " + getPath(next, accountKey);
	}

	/**
	 * Recursively print all the nodes in order from a root node to the standard
	 * output.
	 * @param node The root node to print from.
	 */
	private void dump(Node node) {
		if (node == null)
			return;

		dump(node.left);

		double accountBalance = node.account.getBalance();
		String formattedBalance = String.format(" %.2f", accountBalance);

		System.out.println(node.account.getKey() + formattedBalance);

		dump(node.right);
	}

	// ---------------------------- Helper Methods ----------------------------

	/**
	 * Recursively find the minimum subnode in a node.
	 * @param node The node to get the minimum subnode from.
	 * @return Node The minimum subnode.
	 */
	private Node getMinNode(Node node) {
		if (node == null || node.left == null)
			return node;

		return getMinNode(node.left);
	}
}
