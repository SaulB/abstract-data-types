/**
 * <h1>A bank account.</h1>
 * This Account class is for bank accounts, handling an account number and
 * balance.
 *
 * @author Saul Boyd, 1534999
 */
public class Account {
	// The account number for this account
	private int key;

	// The balance of this account.
	private double balance;

	/**
	 * Create a new account.
	 * @param key The key (account number) for this account.
	 * @param balance The starting balance of this account.
	 */
	public Account(int key, double balance) {
		this.key = key;
		this.balance = balance;
	}

	/**
	 * Create a new account with an empty balance.
	 * @param key The key (account number) for this account.
	 */
	public Account(int key) {
		this(key, 0.0);
	}

	/**
	 * Get the key (account number) for this account.
	 * @return The account's key.
	 */
	public int getKey() {
		return key;
	}

	/**
	 * Get the balance of this account.
	 * @return The account's balance.
	 */
	public double getBalance() {
		return balance;
	}

	/**
	 * Update the balance by a value.
	 * This method only changes the balance by the value.
	 * Positive values will increment the balance and negative values will
	 * decrement it.
	 * @param value The value to update the balance by.
	 */
	public void setBalance(double value) {
		balance += value;
	}
}
