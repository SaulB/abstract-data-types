# Abstract Data Types

Abstract data types for java.<br />
<br />
Compile the Abstract Data Type Test (ADTT) with:
```
javac ADTT.java
```

And run it with:
```
java ADTT
```
