/**
 * <h1>(Min)Heap of Integers</h1>
 * MyMinHeap is an implementation of MinHeapFace to handle a (min)heap of ints.
 *
 * @author Saul Boyd, 1534999
 */
public class Heap {
	// The initial size of the heap.
	private final int INITIAL_SIZE = 20; // >= 4

	// The ratio to resize the array by.
	private final float RESIZE_RATIO = 1.2f; // > 1

	// The array which holds the data.
	private int[] data;

	// The count of items contained in the heap.
	private int size = 0;

	/**
	 * Create a new heap.
	 */
	public MyMinHeap() {
		data = new int[INITIAL_SIZE];
	}

	/**
	 * Add an integer to the heap.
	 * @param item The integer to add.
	 */
	public void add (int item) {
		// Check if we need to resize
		if (size >= data.length)
			resize(getNextSize(data.length));

		data[size] = item;

		upHeap(size);

		size++;
	}

	/**
	 * Get the integer at the top of the heap without removing it.
	 * If the heap is empty it will output a warning to standard error.
	 * @return The integer or -1 if the heap is empty.
	 */
	public int get () {
		if (size != 0)
			return data[0];

		System.err.println("Warning: Heap is empty.");
		return -1;
	}

	/**
	 * Discard the item at the top of the heap.
	 */
	public void remove () {
		if ( isEmpty() )
			return;

		data[0] = data[size - 1];

		size--;

		downHeap(0);

		// Resize if appropriate.
		downsize();
	}

	/**
	 * Replace the top item of the heap and sort.
	 * @param item The integer to replace with.
	 */
	public void replace (int item) {
		if ( isEmpty() ) {
			add(item);

			return;
		}

		data[0] = item;

		downHeap(0);
	}

	/**
	 * Reset the heap to it's initial state.
	 */
	public void clear () {
		size = 0;
		data = new int[INITIAL_SIZE];
	}

	/**
	 * Check if the heap is empty.
	 * @return True if the heap is empty.
	 */
	public boolean isEmpty () {
		return size == 0;
	}

	/**
	 * Get number of items contained in the heap.
	 * @return The size of the heap.
	 */
	public int size () {
		return size;
	}

	// ------------------------------- Reshaping -------------------------------

	/**
	 * Recursively upheap an item at an index to where it belongs.
	 * @param i The index of the item to upheap.
	 */
	private void upHeap (int i) {
		if (i <= 0)
			return;

		// Get the parent.
		int p = (i - 1) / 2;

		if (data[p] <= data[i])
			return;

		swap(i, p);

		upHeap(p);
	}

	/**
	 * Recursively downheap an item at an index to where it belongs.
	 * @param i The index of the item to downheap.
	 */
	private void downHeap (int i) {
		// Check if this node has any children.
		if (i >= size / 2)
			return;

		// Minimum child index. (Initially is left child.)
		int c = 2 * i + 1;

		// If there is 2 children reassign 'c' to the smallest.
		if (size % 2 != 0 || i != (size - 1) / 2)
			c += data[c + 1] < data[c] ? 1 : 0;

		// Check if it is where it belongs.
		if (data[i] <= data[c])
			return;

		swap(i, c);

		downHeap(c);
	}

	// ------------------------------- Resizing -------------------------------

	/**
	 * Attempt to downsize the data array, will only be performed if there is
	 * sufficient room.
	 * This operation will leave enough room as if the array was just upsized.
	 */
	private void downsize () {
		// Don't down size if we are already at minimum size
		if (data.length <= INITIAL_SIZE)
			return;

		// Only downsize if we are lower than the target size
		int targetSize = (int)( data.length / (RESIZE_RATIO * RESIZE_RATIO) );

		if (size > targetSize)
			return;

		// Resize to the next size up
		int nextSize = getNextSize(size);

		// Ensure we don't go below the lower bound.
		if (nextSize < INITIAL_SIZE)
			nextSize = INITIAL_SIZE;

		resize(nextSize);
	}

	/**
	 * Calculate the next size for the data array based on RESIZE_RATIO
	 * @param len The length to calculate the next size from.
	 * @return The next size for the array.
	 */
	private int getNextSize (int len) {
		int newSize = (int)(len * RESIZE_RATIO);

		// Ensure that the new size is bigger for small initial sizes
		if (newSize == len)
			newSize = (newSize < 4) ? 4 : newSize * 2;

		return newSize;
	}

	/**
	 * Resize the data array to the given size.
	 * @param newSize The size to resize it to.
	 * @throws IllegalArgumentException If newSize is smaller than the current
	 * size.
	 */
	private void resize (int newSize) {
		if (newSize < size)
			throw new IllegalArgumentException("\"newSize\" should be greater "
				+ "than \"size\".");

		int[] newData = new int[newSize];

		for (int i = 0; i < size; i++)
			newData[i] = data[i];

		data = newData;
	}

	// -------------------------------- Helpers --------------------------------

	/**
	 * Swap the item at an index with one from another index.
	 * @param i The index of the first item.
	 * @param j The index of the second item.
	 */
	private void swap (int i, int j) {
		data[i] = data[i] ^ data[j] ^ (data[j] = data[i]);
	}
}
