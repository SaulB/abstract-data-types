/**
* <h1>Singly linked string list.</h1>
* The SList class is an implementation of a (singly) linked list for strings.
*
* @author Saul Boyd
*/
public class SList {
	/**
	* A node to hold a string.
	* The SNode implements a node that holds a string for a singly linked
	* list.
	*/
	private class SNode {
		// The value that this node holds.
		private String key;

		// The next node in the list.
		public SNode next = null;

		/**
		* Creates a new node holding a string value.
		* @param s The string to store in this node.
		*/
		public SNode (String s) {
			key = new String(s);
		}

		/**
		* Get the string value this node holds.
		* @return String The string value of this node.
		*/
		public String getValue() {
			return key;
		}
	}

	// The total count of the nodes in the list.
	private int count;

	// The first node in the list.
	private SNode head = null;

	/**
	* Add a string to the start (head) of the list.
	* Empty strings will be added, null string objects will return without
	* being added.
	* @param s The string to add.
	*/
	public void add(String s) {
		if (s == null)
			return;

		SNode newHead = new SNode(s);

		newHead.next = head;
		head = newHead;

		count++;
	}

	/**
	* Check to see if this list contains a string.
	* @param s The string to check for.
	* @return Boolean True if the list contains that string.
	*/
	public Boolean has(String s) {
		SNode currNode = head;

		while (currNode != null) {
			if (currNode.getValue().equals(s))
				return true;

			currNode = currNode.next;
		}

		return false;
	}

	/**
	* Remove a string from the list.
	* @param s The string to remove.
	*/
	public void remove(String s) {
		if (isEmpty() || s == null)
			return;

		// Special case for the head.
		if (head.getValue().equals(s)) {
			head = head.next;

			count--;
			return;
		}

		// All other cases.
		SNode currNode = head.next;
		SNode prevNode = head;

		while (currNode != null) {
			if (currNode.getValue().equals(s)) {
				prevNode.next = currNode.next;

				count--;
				return;
			}

			prevNode = currNode;
			currNode = currNode.next;
		}
	}

	/**
	* Get the total number of strings contained in the list.
	* @return int The number of strings.
	*/
	public int length() {
		return count;
	}

	/**
	* Check to see if the list is empty.
	* @return Boolean True if the list has any strings otherwise false.
	*/
	public Boolean isEmpty() {
		return count == 0;
	}

	/**
	* Dump the list to the standard output.
	* This will output all the strings in the list in the reverse order from
	* which they were added.
	*/
	public void dump() {
		SNode currNode = head;

		while (currNode != null) {
			System.out.println(currNode.getValue());

			currNode = currNode.next;
		}
	}
}
