import java.util.Arrays;

class ADTT {
	public static void main (String[] args) {
		// Creating a set
		Set<Integer> set = new Set<Integer>();

		// Adding items
		for (int i = 0; i < 100; i++)
			set.insert(i);

		set.insert(1);
		set.insert(99);
		set.insert(0);
		set.insert(100);


		// Removing items
		for (int i = 25; i < 75; i++)
			set.remove(i);

		// Union
		Set<Integer> set2 = new Set<Integer>();

		for (int i = 0; i < 50; i++)
			set2.insert(i);

		set.union(set2);

		// Intersection
		Set<Integer> set3 = new Set<Integer>();

		for (int i = 0; i < 100; i+=2)
			set3.insert(i);

		set.intersection(set3);

		// Convert to an array of Integers
		Integer[] setArr = new Integer[set.size()];
		setArr = set.toArray(setArr);

		// Convert to an array of Objects
		Object[] setObjArr = set.toArray();

		System.out.println(Arrays.toString(setObjArr));
	}
}
