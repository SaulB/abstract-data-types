/**
* This Set class implements a set of numbers.
* The set will contain no duplicates.
* Order is not guaranteed.
* Sets are not type safe.
*
* @author Saul Boyd
*/
class Set<T> {
	// Default size of the data array.
	private final int defaultSize = 4;

	// The rate in which the array will grow.
	private final int growthRate = 2;

	// The array which makes up our set.
	private T[] data;

	// The count items in our data.
	private int count;

	public Set() {
		count = 0;

		// Damn generics workaround.
		@SuppressWarnings("unchecked")
		T[] tempData = (T[]) new Object[defaultSize];

		data = tempData;
	}

	/**
	* Pure - Get the number of elements in the set.
	* @return int This is the number of elements in the set.
	*/
	public int size () {
		return count;
	}

	/**
	* Insert an item into the set.
	* Will only inset the item if it is not alread in the set.
	* @param item The item to insert into the set.
	*/
	public void insert (T item) {
		if (!contains(item)) {
			// Grow if needed.
			checkSize();

			data[count] = item;
			count++;
		}
	}

	/**
	* Remove an item from the set.
	* @param item The item to remove from the set.
	* @return The item that was removed or null if it was not found in the set.
	*/
	public T remove(T item) {
		for (int i = 0; i < count; i++) {
			if (data[i] == item) {
				T tempItem = item;

				// Move the last element into the items position.
				data[i] = data[count - 1];

				count--;

				// Shrink if needed.
				checkSize();

				return item;
			}
		}

		return null;
	}

	/**
	* Pure - Find an element in the set.
	* @param item The item to look for.
	* @return The item if it is in the set otherwise null.
	*/
	public T find (T item) {
		for(int i = 0; i < count; i++) {
			if (data[i] == item)
				return item;
		}

		return null;
	}

	/**
	* Pure - Check if the set contains an element.
	* @param item The item to check for.
	* @return True if the item is in the set.
	*/
	public Boolean contains (T item) {
		return find(item) != null;
	}


	/**
	* Modify this set to become a union of this and the other set(s).
	* @param otherSets The set(s) to union with.
	*/
	public void union (Set... otherSets) {
    	for (Set other : otherSets) {
			Object[] otherArray = other.toArray();

			for (int i = 0; i < other.size(); i++) {
				@SuppressWarnings("unchecked")
				T otherAsT = (T) otherArray[i];

				insert(otherAsT);
			}
		}
	}

	/**
	* Create a set from the union of sets.
	* @param sets The sets to form a union from.
	* @return The set that was formed from the union of sets.
	*/
	public static Set createUnion(Set... sets) {
		Set output = new Set();

		for (Set set : sets) {
			output.union(set);
		}

		return output;
	}

	/**
	* Modify this set to become the intersection of this and the other set(s).
	* @param otherSets The set(s) to intersect with.
	*/
	public void intersection(Set... otherSets) {
		// Setup an array to store the uniue items
		@SuppressWarnings("unchecked")
		T[] toRemove = (T[]) new Object[data.length];
		int toRemoveIndex = 0;

    	for (Set other : otherSets) {
			for(int i = 0; i < count; i++) {
				@SuppressWarnings("unchecked")
				Boolean unique = !other.contains(data[i]);

				if (unique) {
					toRemove[toRemoveIndex] = data[i];
					toRemoveIndex++;
				}
			}
		}

		// Remove the unique items.
		for (int i = 0; i < toRemoveIndex; i++) {
			remove(toRemove[i]);
		}
	}

	/**
	* Create a new set from the intersection of sets.
	* @param sets The sets to form a intersection from.
	* @return The set that was formed from the intersection of sets.
	*/
	public static Set createIntersection(Set... sets) {
		Set output = sets[0];

		for (int i = 1; i < sets.length; i++) {
			output.intersection(sets[i]);
		}

		return output;
	}

	/**
	* Convert the set to an array using an existing array.
	* @param array The array to poulate with the set, should be the size of the
	* set, otherwise it will fill only up to the set or array's limit.
	* @return The modified array.
	*/
	public T[] toArray(T[] array) {
		int maxSize = array.length < count ? array.length : count;

		for(int i = 0; i < maxSize; i++)
			array[i] = data[i];

		return array;
	}

	/**
	* Pure - Convert the set to an array of objects.
	* @return The array of objects that are contained in the set.
	*/
	public T[] toArray() {
		@SuppressWarnings("unchecked")
		T[] output = (T[]) new Object[count];

		for(int i = 0; i < count; i++)
			output[i] = data[i];

		return output;
	}

	/**
	* Pure - Convert each element into a string seperated by newlines.
	* @return String This is the string of all the elements.
	*/
	public String toString() {
		String output = "";

		for(int i = 0; i < count; i++)
			output += data[i].toString() + "\n";

		return output;
	}

	/**
	* Checks the size of the data array and resizes if needed.
	*/
	private void checkSize() {
		// Grow if needed.
		if (count == data.length) {
			resize(data.length * growthRate);

			return;
		}

		// Shrink if we less than half the previous rate
		int rate = data.length / (growthRate * 2);
		int newMax = data.length / growthRate;

		if (newMax >= defaultSize) {
			if (count <= rate)
				resize(newMax);
		}
	}


	/**
	* Resizes the data to the specified size.
	* @throws Exception in the case of the new size being less than the count.
	*/
	private void resize (int size) {
		if (count > size) {
			throw new IllegalArgumentException("The \'size\' parameter " +
				"should not be less than the count of elements.");
		}

		@SuppressWarnings("unchecked")
		T[] tempData = (T[]) new Object[size];

		// Copy the old arr to the new one.
		for (int i = 0; i < count; i++)
			tempData[i] = data[i];

		data = tempData;
	}
}
